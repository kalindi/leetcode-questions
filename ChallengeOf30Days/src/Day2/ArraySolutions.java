package Day2;

import java.util.Arrays;

public class ArraySolutions {
	public int nthSuperUglyNumber0(int n, int[] primes) {
        long[] uglyNums = new long[n];
        int[] idx = new int[primes.length];
        uglyNums[0] = 1;

        for(int i = 1; i < n; i++) {
            uglyNums[i] = Integer.MAX_VALUE;

            for(int j = 0; j < primes.length; j++) {
                long val1 = primes[j] * uglyNums[idx[j]];

                uglyNums[i] = Math.min(uglyNums[i], val1);
            }
            
            //This iteration is to increase the index (of ugly[])of the prime whose multiplication gave the next ugly number.
            for(int j = 0; j < primes.length; j++) {
                if(uglyNums[i] == primes[j] * uglyNums[idx[j]]) {
                    idx[j]++;
                }
            }
        }
        return (int)uglyNums[uglyNums.length - 1];
    }
	
	public int nthSuperUglyNumber1(int n, int[] primes) {
        long[] uglyNums = new long[n];
        int[] idx = new int[primes.length];
        long[] valuesOfPrimes = new long[primes.length];
        Arrays.fill(valuesOfPrimes, 1);
        long next = 1;

        for(int i = 0; i < n; i++) {
            uglyNums[i] = next;

            next = Integer.MAX_VALUE;

            for(int j = 0; j < primes.length; j++) {
                //In first iteration all of primes will be multiplied with 1 and ready for their next idx. Waiting for their turn
                //As values are already stored in array of size equal to primes.length, 
            	//one iteration will take care of comparison, assignment as well as index increment.
                //This is possible only because we know the first ugly number.
                if(valuesOfPrimes[j] == uglyNums[i]) {
                    valuesOfPrimes[j] = primes[j] * uglyNums[idx[j]++]; 
                }
                next = Math.min(next, valuesOfPrimes[j]);
            }
        }
        return (int)uglyNums[uglyNums.length - 1];
    }
}
