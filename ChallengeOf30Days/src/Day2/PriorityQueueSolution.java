package Day2;

import java.util.PriorityQueue;

public class PriorityQueueSolution {
	public int nthSuperUglyNumber(int n, int[] primes) {
        int[] ugly = new int[n];
        ugly[0] = 1;

        PriorityQueue<int[]> pq = new PriorityQueue<>((a,b) -> a[0] - b[0]);
        for (int i = 0; i < primes.length; i++) {
            //Storing first iteration after multiplying each prime with 1st ugly number 1.
            //a[0] = multiplication to get next ugly number for that prime
            //a[1] = prime number
            //a[2] = idx of ugly number for that prime 
            pq.offer(new int[]{primes[i], primes[i], 1});
        }

        for (int i = 1; i < n; i++) {
            ugly[i] = pq.peek()[0];
            //to increase idx of all duplicates
            while(pq.peek()[0] == ugly[i]) {
                int[] vals = pq.poll();
                pq.offer(new int[]{vals[1] * ugly[vals[2]], vals[1], vals[2] + 1});
            }           
        }

        return ugly[n - 1];
    }
}
