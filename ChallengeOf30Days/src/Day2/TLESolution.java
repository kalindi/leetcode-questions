package Day2;

import java.util.ArrayList;
import java.util.List;

public class TLESolution {
	public int nthSuperUglyNumber(int n, int[] primes) {
        if(n == 1) {
            return 1;
        }
        int num = 2;
        int ind = 2;

        List<Integer> superUgly = new ArrayList<>();
        superUgly.add(1);
        
        popuateSuperUglyNumbers(num, n, primes, superUgly, ind);

        System.out.println(superUgly);
        
        return superUgly.get(n - 1); 
    }

    public void popuateSuperUglyNumbers(int num, int n, int[] primes, List<Integer> superUgly, int ind) {

        if(superUgly.size() >= n) {
            return;            
        }
        
        List<Integer> primeFactors = new ArrayList<>();

        getPrimeFactors(num, ind, primeFactors);
        boolean found = false;

        for(int i = 0; i < primeFactors.size(); i++) {
            for(int j = 0; j < primes.length; j++) {
                if(primes[j] == primeFactors.get(i)) {
                    found = true;
                }
            }

            if(!found || i == primeFactors.size() - 1) {
                break;
            }
            
            found = false;
        }

        if(found) {
            superUgly.add(num);
        }

        popuateSuperUglyNumbers(num + 1, n, primes, superUgly, ind);
    }

    public void getPrimeFactors(int num, int ind, List<Integer> primeFactors) {
        if(ind > num || num < 2) {
            return;
        }

        if(num % ind == 0) {
            num = num/ind;
            primeFactors.add(ind);
            getPrimeFactors(num, 2, primeFactors);
        } else {
            getPrimeFactors(num, ind + 1, primeFactors);
        }
    }
}
