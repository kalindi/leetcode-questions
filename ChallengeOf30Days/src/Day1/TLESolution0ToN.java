package Day1;

public class TLESolution0ToN {
	public int numDecodings(String s) {
        if (s.charAt(0) == '0') {
                return 0;
        }
        return numDecodingsUtil(s, 0);
    }

    public int numDecodingsUtil(String s, int ind) {

        if(ind > s.length() - 1) {
            return 1;
        }

        int res = 0;
        if(ind < s.length() - 1 && (s.charAt(ind) == '1' || (s.charAt(ind) == '2' && s.charAt(ind + 1) < '7'))) {
            res += numDecodingsUtil(s, ind + 2); 
        }

        if(s.charAt(ind) != '0') {
            res += numDecodingsUtil(s, ind + 1);
        }

        return res;
    }
}
