package Day1;

import java.util.Arrays;

public class Memoization {
	public int numDecodings(String s) {
        if (s.charAt(0) == '0') {
            return 0;
        }
        int[] memo = new int[s.length()];
        Arrays.fill(memo, -1);
        return numDecodingsUtil(s, s.length() - 1, memo);
    }

    public int numDecodingsUtil(String s, int ind, int[] memo) {

        if(ind <= 0) {
            return 1;
        }

        if(memo[ind] != -1) {
            return memo[ind];
        }

        int res = 0;
        if(s.charAt(ind - 1) == '1' || (s.charAt(ind - 1) == '2' && s.charAt(ind) < '7')) {
            res += numDecodingsUtil(s, ind - 2, memo); 
        }

        if(s.charAt(ind) != '0') {
            res += numDecodingsUtil(s, ind - 1, memo);
        }
        memo[ind] = res;
        return memo[ind];
    }
}
