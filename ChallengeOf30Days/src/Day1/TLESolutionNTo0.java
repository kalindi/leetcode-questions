package Day1;

public class TLESolutionNTo0 {
	public int numDecodings(String s) {
        if (s.charAt(0) == '0') {
            return 0;
        }
        return numDecodingsUtil(s, s.length() - 1);
    }

    public int numDecodingsUtil(String s, int ind) {

        if(ind <= 0) {
            return 1;
        }

        int res = 0;
        if(s.charAt(ind - 1) == '1' || (s.charAt(ind - 1) == '2' && s.charAt(ind) < '7')) {
            res += numDecodingsUtil(s, ind - 2); 
        }

        if(s.charAt(ind) != '0') {
            res += numDecodingsUtil(s, ind - 1);
        }

        return res;
    }
}
