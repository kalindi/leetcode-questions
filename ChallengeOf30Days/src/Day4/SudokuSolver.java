package Day4;

public class SudokuSolver {

    public void solveSudoku(char[][] board) {
        sudokuUtil(board);
    }

    public boolean sudokuUtil(char[][] board) {

        for(int row = 0; row < 9; row++) {
            for(int col = 0; col < 9; col++) {

                if(board[row][col] == '.') {
                	//Try chars from 1 to 9 for that cell
                    for(char val = '1'; val <= '9'; val++) {

                        if(isValid(board, row, col, val)) {
                            board[row][col] = val;

                            if(sudokuUtil(board)) {
                                return true;                               
                            }
                            
                            //If false is returned from recursive call,
                            //backtrack and set value again to '.' to try other solution.
                            board[row][col] = '.';
                        } 
                    }
                    //If no char qualifies, we need to return false to backtrack and try again
                    return false;
                }
            }
        }
        //If all values are filled and board is valid.
        return true;
    }

    public boolean isValid(char[][] board, int row, int col, char val) {
        for(int i = 0; i < 9; i++) {
        	//Check vertically
            if(board[i][col] == val) {
                return false;
            }
            //Check horizontally
            if(board[row][i] == val) {
                return false;
            }
            //Check box
            if(board[3*(row/3) + (i/3)][3*(col/3) + (i%3)] == val) {
                return false;
            }
        }
        return true;
    }

}
