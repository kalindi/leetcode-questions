package Day3;

import java.util.Arrays;

public class TLERecursiveApproach {
	public int lengthOfLIS(int[] nums) {
        return lengthOfLISUtil(nums, 0, -1);
    }

    public int lengthOfLISUtil(int[] nums, int curr, int prev) {
        
        if(curr >= nums.length) {
            return 0;
        }

        int inc = 0;
        int exc = 0;
        
        //pick branch
        //-1 is the starting condition
        if(prev == -1 || nums[curr] > nums[prev])
            inc = 1 + lengthOfLISUtil(nums, curr + 1, curr);
        //no pick branch
        exc = lengthOfLISUtil(nums, curr + 1, prev);

        return Math.max(inc,exc); 
    }
}
