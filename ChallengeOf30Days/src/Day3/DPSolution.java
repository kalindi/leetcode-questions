package Day3;

import java.util.Arrays;

public class DPSolution {
	public int lengthOfLIS(int[] nums) {
        int[] dp = new int[nums.length];
        Arrays.fill(dp, 1);

        for(int i = 1; i < nums.length; i++) {
            for(int j = 0; j < i; j++) {
                if(nums[j] < nums[i]) {
                    dp[i] = Math.max(dp[j] + 1, dp[i]);
                }
            }
        } 

        int ans = 0;

        for(int i = 0; i < dp.length; i++) {
            ans = Math.max(dp[i], ans);
        }

        return ans;
    }
}
