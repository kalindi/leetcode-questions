package Day3;

import java.util.Arrays;

public class MemoisationSolution {
	public int lengthOfLIS(int[] nums) {
		//2d array for memoisation
        int[][] dp = new int[nums.length][nums.length];
        for(int i = 0; i < dp.length; i++) {
            Arrays.fill(dp[i], -1);
        }

        return lengthOfLISUtil(nums, 0, -1, dp);
    }

    public int lengthOfLISUtil(int[] nums, int curr, int prev, int[][] dp) {
        
        if(curr >= nums.length) {
            return 0;
        }
        
        //returning from here only if sub problem is already visited.
        if(prev != -1 && dp[curr][prev] != -1) {
            return dp[curr][prev];
        }

        int inc = 0;
        int exc = 0;
        
        //-1 is the starting condition
        if(prev == -1 || nums[curr] > nums[prev])
            inc = 1 + lengthOfLISUtil(nums, curr + 1, curr, dp);

        exc = lengthOfLISUtil(nums, curr + 1, prev, dp);

        int ans = Math.max(inc,exc);
        
        //Storing the value for a particular combination to avoid calculating it again when sub problem repeats in recursion.
        if(prev != -1) {
            dp[curr][prev] = ans;
        }

        return ans;
    }
}
