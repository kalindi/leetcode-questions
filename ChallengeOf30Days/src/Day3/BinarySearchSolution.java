package Day3;

public class BinarySearchSolution {
	public int lengthOfLIS(int[] nums) {
        int[] seqSortedArr = new int[nums.length];
        int length = 0;

        for (int i = 0; i < nums.length; i++) {
        	
            int left = 0, right = length; 
            while (left < right) {
            	
                int mid = left + (right - left)/2;
                
                if (seqSortedArr[mid] >= nums[i]) {
                    right = mid;
                } else {
                    left = mid + 1;
                }
            }

            seqSortedArr[left] = nums[i];
            
            if (left == length) {
                length++;
            }
        }

        return length;
    }
}
